  var app = angular.module('myApp', []);
		app.controller('myCtrl', ['$scope', '$http', function($scope, $http){

    //$scope.submit = true;
    //$scope.update = true;
      //Table data
      $http.get("includes/userdetails.php").then(function (response) {
        $scope.tabledata = response.data;
      });

      $scope.formView = false;
      $scope.formFunc = function(){
        $scope.message = "Registration Form";
        $scope.isUser = true;
        $scope.formView = !$scope.formView;
        $scope.Isdisable = true;
      };

      $scope.formOff = function(){
        $scope.formView = false;
        $scope.Isdisable = false;
        $scope.entry = {};
      };
      
      //form submit
			$scope.submitForm = function() {
        $http({
          method  : 'POST',
          url     : 'includes/userentry.php',
          data    : $scope.entry,
          headers : { 'Content-Type': 'application/json' } 
        }).success(function(data) {
            console.log("Data Inserted Successfully", data);
          });
      };

      //Delete
      $scope.deleteUser = function(detail){
        if(confirm("Are sure you want to delete?")) {
          $http.post('includes/deleteuser.php',{"id":detail.user_id}).then(function(data){
            console.log("Successfully Deleted.");
            var index = $scope.tabledata.indexOf(detail);
            $scope.tabledata.splice(index, 1);
          });
        }
        else {
          return false;
        }
      };
      $scope.entry = {};
      $scope.editInfo = function(detail){
        $scope.message = "Edit Info";
        $scope.isUser = false;
        $scope.submit = false;
        $scope.update = false;
        $scope.entry = detail;
        $scope.formView = !$scope.formView;
      };
      $scope.updateInfo = function(){
        $http({
          method  : 'POST',
          url     : 'includes/useredit.php',
          data    : $scope.entry,
          headers : { 'Content-Type': 'application/json' } 
        }).success(function(data) {
          console.log("Data Inserted Successfully", data);
          $scope.entry = {};
        });
      };
	}]);